var tamaño = 3,
  vacio = '&nbsp;',
  cajas = [],
  turno = 'X',
  puntuacion,
  mueve;

  /**
  * Inicializa el tablero y comienza el juego.
  */
function init() {
  var tablero = document.createElement('table');
  tablero.setAttribute('border', 1);
  tablero.setAttribute('cellspacing', 0);

  var identificador = 1;
  for (var i = 0; i < tamaño; i++) {
    var row = document.createElement('tr');
    tablero.appendChild(row);
    for (var j = 0; j < tamaño; j++) {
      var cell = document.createElement('td');
      cell.setAttribute('height', 120);
      cell.setAttribute('width', 120);
      cell.setAttribute('align', 'center');
      cell.setAttribute('valign', 'center');
      cell.classList.add('col' + j, 'row' + i);
      if (i == j) {
        cell.classList.add('diagonal0');
      }
      if (j == tamaño - i - 1) {
        cell.classList.add('diagonal1');
      }
      cell.identificador = identificador;
      cell.addEventListener('click', set);
      row.appendChild(cell);
      cajas.push(cell);
      identificador += identificador;
    }
  }

  document.getElementById('tresenraya').appendChild(tablero);
  juegocomienza();
}

/**
 * Juego nuevo
 */
function juegocomienza() {
  puntuacion = {
    'X': 0,
    'O': 0
  };
  mueve = 0;
  turno = 'X';
  cajas.forEach(function (square) {
    square.innerHTML = vacio;
  });
}

/**
 * Compruebe si gana o no
 */
function ganar(clicked) {
  // Recibe todas las clases
  var memberOf = clicked.className.split(/\s+/);
  for (var i = 0; i < memberOf.length; i++) {
    var testClass = '.' + memberOf[i];
    var items = contains('#tresenraya ' + testClass, turno);
    // condición ganadora: girar == tamaño
    if (items.length == tamaño) {
      return true;
    }
  }
  return false;
}

/**
 * Función auxiliar para comprobar si NodeList del selector tiene un texto particular
 */
function contains(selector, text) {
  var elements = document.querySelectorAll(selector);
  return [].filter.call(elements, function (element) {
    return RegExp(text).test(element.textContent);
  });
}

/**
 * Establece el cuadrado pulsado y también actualiza el turno.
 */
function set() {
  if (this.innerHTML !== vacio) {
    return;
  }
  this.innerHTML = turno;
  mueve += 1;
  puntuacion[turno] += this.identificador;
  if (ganar(this)) {
    alert('GANADOR: JUGADOR ' + turno);
    juegocomienza();
  } else if (mueve === tamaño * tamaño) {
    alert('EMPATE');
    juegocomienza();
  } else {
    turno = turno === 'X' ? 'O' : 'X';
    document.getElementById('turno').textContent = 'JUGADOR ' + turno;
  }
}

init();